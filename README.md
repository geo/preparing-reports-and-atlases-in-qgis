# Creating education statistical yearbooks with QGIS

One of the responsibilities of a Ministry of Education is to produce high quality, up-to-date reports that reflect the performance of the education system over a school year. The dissemination of statistical yearbooks or reports (and the data therein) is fundamental to inform policy-making, as well as to maintain an optimal level of transparency with stakeholders and the general population. 

In general, statistical yearbooks are produced once a year based on administrative information compiled by the Ministry, typically through their Education Management Information System (EMIS). While the information is accessible to the officers from the EMIS and planning teams, the transformation of this information into publishable reports can take a lot of time since it requires extracting countless tables, and generating charts and maps, not to mention producing analytical text. 

That said, the production of a statistical yearbook takes place at the very end of the data production cycle, which means that disseminating timely educational data might be a challenge if there were bottlenecks throughout the cycle, and especially if other, more pressing activities are crowding the EMIS team agenda. In some countries, the staff involved in producing these reports may come from various teams, which might also add to the delays. 

The following technical note is a brief guide on how to produce semi-automatized statistical reports that include data tables, charts, and maps, based on national data produced from the education system, and using a free, open-source GIS software: QGIS. 

The approach put forward here works regardless of the EMIS system in place, and it is aimed at streamlining the transformation of available data into the publication of high-quality reports, hoping to free the precious time of planners to work on analysis, or on improving data quality and other processes if needed. 

Target users for this note need to be comfortable using QGIS 3.X, as well as have a basic level of understanding of how to search the web. This guide is not intended as a replacement for efficient report production processes already in place, but it can serve to speed up mechanical work, particularly in contexts where data is already available but where staff have competing priorities. 

The note presents the different steps to prepare the necessary information for producing the automatized reports, to set up QGIS and import data, to generate a sample of maps, tables and charts, and finally to put together the final reports or atlases. For example, Figure 1 shows a typical Atlas, where a single visual can iterate over a number of territories (in this case showing multiple databases for each State of Myanmar), while Figure 2 shows a report from Sierra Leone, where multiple topics are addressed in different sections of the automatized report.

**Figure 1. Example of Atlas**

![Figure 1](https://gitlab.iiep.unesco.org/g.vargas/preparing-reports-and-atlases-in-qgis/blob/main/assets/Figure_1.png?raw=true "Figure 1")

**Figure 2. Example of Report**

![Figure 2](https://gitlab.iiep.unesco.org/g.vargas/preparing-reports-and-atlases-in-qgis/blob/main/assets/Figure_2.png?raw=true "Figure 2")

## Step 1. Preparing the necessary information

The main source of information used in statistical yearbooks is the administrative data available through the EMIS. It contains the most up-to-date, disaggregated, relevant information on the education system, and it has followed all the quality criteria set by the government for statistical data production. From the information contained within the EMIS, it is possible to calculate almost all indicators of interest for education planners and managers, and in-depth analysis especially if the information is georeferenced. It is possible to use additional types of data (e.g. TMIS, financial data, etc.) as long as there exists a unique identifier for all databases (e.g. school ID, municipality code, etc.).

In order to get the information in the suitable format, it is necessary to recognize the different databases that can be used from the EMIS repository or from other data sources. These databases can have different repositories for schools, students, teaching and non-teaching staff. As long as all databases have keys that can link one piece of information to another (e.g. knowing which students attend which school), planners can obtain the desired information by collapsing, mixing and merging these databases. 

Before starting the production of the automatized reports and atlases, planners should already have a very clear idea of the overall outline of the report, the indicators they want to present, and which maps they want to produce. Then, datasets will be prepared to represent the entirety of the information to be presented in the report, including GIS information for maps. For the purpose of this technical note, databases should be then organized in two possible ways:

- If the database has geographic information using a spatial reference system, then the file should be organized in such a way that each record includes a variable for the latitude coordinates and a variable for the longitude coordinates. This should then be converted to a CSV file and added to QGIS through Layer --> Add Layer --> Add Delimited Text Layer. See [Example file 1](https://box.iiep.unesco.org/s/DRMSQBgCdxTWgz6).
- If the information available does not include precise geographic references (e.g. coordinates for schools are not available, but school information is aggregated at the District level) then the file should be organized so that each entry (each row) is a single record, with one geographic variable being the key to connect the file with the pre-existing shapefile to be attached to (e.g. the districts used in the education data will have the same geographic boundaries than in the shapefile). See [Example file 2](https://box.iiep.unesco.org/s/nB8E2JNSsfMLKEK).

The above steps are necessary to harness the true power of QGIS: the information needs to be organized in a way that allows the program to identify its location in the file. This means that the databases(regardless whether the information is organized on one of the two ways described above), need to have all the information that planners want to present, such as school characteristics or region/district-level indicators, in subsequent columns within the file. Likewise, each unique observation (be it schools, administrative divisions, etc.), should be a row. Note that a number of additional rules need to be followed for the program recognize the information being used:
- All columns should be named, preferably with short titles without spaces in between words (generally an underscore is used to mark the space and facilitate readership). 
- There should not be merged cells of any kind.
- There should not be comments or information at the bottom of the database.
- Numerical variables should only have numbers (i.e. avoid the use of N/A, n.a., etc.)
- Be cautious when using special characters, such as backslashes (\) and forward slashes (/), as it might cause issues with the internal code of QGIS.
- Avoid naming columns using numbers at the beginning of the title (i.e. use Y1999 rather than 1999). 

Both types of databases should be clearly labelled, with no empty lines, and stored in Comma Separated Values (CSV) format. 
Apart from EMIS data, there are multitudes of additional data sources that can be used to enrich and contextualize maps. The National Statistical Office might produce a range of datasets that include geographic information, and many y additional data sources can be added to nationally produced information. Some of the additional data sources include:

- [Humanitarian Data Exchange - HDX](https://data.humdata.org/)
- [Socioeconomic Data and Applications Center – SEDAC](https://sedac.ciesin.columbia.edu/)  
- [WorldPop](https://www.worldpop.org/)  
- [Copernicus](https://www.copernicus.eu/en) 
- [Displacement Tracking Matrix – DTM](https://dtm.iom.int/)
- [Armed Conflict Location & Event Data Project – ACLED](https://acleddata.com/)

The following instructions use information freely available online from a multitude of websites. 
1.	Go to the [Humanitarian Data Exchange - HDX](https://data.humdata.org/) and, on the section Find data, add the country of interest.
2.	Download all relevant information, including administrative boundaries.
3.	Store all information in two separate sub-folders within the same country folder, called Databases and Shapefiles. All information not formatted as shapefile, such as CSV files, should be stored on the Database sub-folder. 

## Step 2. Setting up and import the information into QGIS

In order to have projects that can be easily shared between computers and across the internet, it is important to use GeoPackages rather than Shapefiles. Additionally, properly setting up the information into QGIS allows for a streamline d process that will later result in Atlases and Reports (Atlases are documents that iterate through a single variable, while Reports allow for multiple Atlases, with iterations nested within other iterations, and adding static pages, cover pages and end pages) that present information dynamically. 

Once the information has been prepared, the planner needs to determine the kind of information to be presented on the Atlas or Report. Is the purpose of the exercise to present a single map, a series of maps of the same area with different variables, the same information for a multitude of sub-regions or a full-fledged report? The answer to this question will guide the steps to follow. 

1.	Open QGIS. This software is freely available and can be downloaded here: https://qgis.org/en/site/forusers/download.html and a basic step-by-step training to use QGIS for Educational Planning is available on the [IIEP website](https://iiepdev.gitbook.io/training/self-paced-trainings/qgis/qgis-training-manual-en). 
2.	Begin by adding the most granular level of administrative boundaries. These files usually come with the names and codes for all higher-level administrative regions (i.e. districts have the information of their own names and codes and that of the State where they are located). This information will be useful to create relations between point shapefiles (e.g. schools, violent events, refugee camps, etc.) and administrative boundaries, which in turn allows Expressions to automatically update when using the print composer or when parsing through an atlas or a report. 
3.	Once this file is on QGIS, export it to a new GeoPackage. Save the project to the same GeoPackage. This will allow the whole project to be shared with others easily.

Before adding the rest of the information, regardless of it being a shapefile or a database (e.g. CSV files with information on schools or districts extracted from EMIS) that needs to be converted, the user needs to determine if the new data source to be added is to be displayed using DataPlotly, using Expressions or with selective masking. The process for each scenario is different: 

If the data **is not** to be displayed using DataPlotly, using Expressions or with selective masking:

* For shapefiles:
    * Go to the containing folder on the Browser section of QGIS and right-click on the desired shapefile. 
    * Select Export Layer --> To File, and select the appropriate GeoPackage.
* For CSVs:
    * For cases when the CSV is georeferenced, add the information normally. When the new layer is on QGIS, export it to the GeoPackage.
    * For cases when the CSV is not georeferenced, but rather the information will be used with the algorithm *Join to a shapefile*, only make the Join after the CSV file has been exported into the GeoPackage.

If the data **is** to be displayed using DataPlotly, using Expressions or with selective masking, then:

* Add the shapefile or CSV-based file into QGIS without adding it to the GeoPackage.
* Use the *Join attributes by location* Processing function within Vector --> Data Management Tools to add the information on the location of each element in relation to the administrative boundaries. The most granular administrative boundary should be used, since this already contains the information of the more general ones. The Processing algorithm will produce an output layer, which can be directly saved on the GeoPackage by selecting the option *Save to GeoPackage* on the *Joined layer* dialog.
* Go to Project --> Properties --> Relations and create relations between the administrative boundaries that are to be parsed through and the layers that joined attributes by location, making sure to use the correct level to make the connection. 

4.	Use the multiple styling options to customize the map with the different layers. If the user only wants to work with multiple views that use repeated layers with different styling options, it is recommended to create groups within the Layers panel using duplicates from the desired layers, and format them individually. Once all the customization is ready, use Themes to create the preferred views with the selected groups. 

## Step 3. Creating Basic maps

The simplest way to present basic map and present simple data, is to use the Print composer by adding the map and its auxiliary elements, such as the title, legend, scale bar and north arrow. 

It might also be interesting to show elements such as totals, means or counts of multiple shapefiles, in order to show valuable pieces of information captured by the EMIS. To do this, the most important Expression is aggregate or relation_aggregate, which can be used with format_number to present summary statistics on a map or to style a layer using information from other layers connected through a Relation. Table 1 presents a series of examples with their expected result.

**Table 1. Examples of the aggregate Expression for basic maps**

|   Formula|Results   |
|---|---|
|  ```aggregate('Conflict data', 'sum', “Fatalities”)``` |  Total number of fatalities on the Conflict data point shapefile |
|  ```format_number(aggregate('IDP camps', 'mean', “IDP_ind”),0)``` | Average number of individuals per IDP camp, formatted to have 0 decimal points |
| ```concat('Total number of violent events in the year: ' , format_number(aggregate('Conflict data','count', “fid”),0) )``` | Number of violent events, formatted, together with a phrase.  |
| ```concat( 'Schools in the region of Valle: ' , format_number(aggregate('Schools','count', “fid”,'REGION' = “Valle”),0))``` |  Number of schools in the Valle region, formatted, together with a phrase |
|  ```relation_aggregate('Administrative boundaries - Level 3 - Primary school','sum', “total_pri_enrolcombine”)/ "Pop_Pri_All" *100``` |  Total number of primary students currently enrolled in each administrative unit at level 3, divided by the corresponding number of primary-aged children, multiplied by 100. |
| ```relation_aggregate('Administrative boundaries – Level 3 – IDP camps', 'mean', “IDP_Ind”)```  | Average number of people in IDP camps in each administrative unit at level 3  |

## Step 4. Generating Atlas and Reports

Atlases and Reports are two powerful features of QGIS that can add a lot of value and which heavily reduce the amount of time needed to produce extensive and detailed documents. An Atlas is a way to rapidly generate a high number of maps and graphs by iterating over a specific variable (e.g. States within a country). Reports are a more potent version of the Atlas, since they allow for Atlas-like sections, as well as static pages, or even Atlases within Atlases (e.g. States within a country, and Counties within each State). Within both options, two additional elements can give value to the way information is presented:

### QGIS Expressions

As in the Basic maps subsection, Expressions can be used to give information on layers of information . This can be done by using filters. Table 2 presents a series of examples with their expected result. Note that in all these examples, the admin2Name was the variable used as the unit to parse through.

**Table 2. Examples of QGIS expressions when constructing Atlases or Reports**

|  Formula | Result  |
|---|---|
| ```concat('Total number of schools: ' , format_number( aggregate( 'Instructional_materials' ,'count', “fid” ,"admin2Name" = attribute(@atlas_feature, 'admin2Name')),0) )``` |  Total number of schools within the current State in the Atlas (or Report), formatted, together with a phrase. |
|  ```concat( 'Indigenous: ' , format_number( aggregate('Instructional_materials','count', “fid” ,"nur_lang_instruct" = 'Indigenous' AND "admin2Name"= attribute( @atlas_feature,'admin2Name')),0)  , ' (', format_number( aggregate('Instructional_materials','count', “fid” ,"nur_lang_instruct" = 'Indigenous' AND "admin2Name"= attribute(@atlas_feature,'admin2Name'))/ aggregate('Instructional_materials','count', “fid”, "admin2Name" = attribute(@atlas_feature,'admin2Name'))*100,2) , '%)')``` |  Number of schools where the instruction material in nurseries is in an indigenous language, for the schools within the current State in the Atlas (or Report), showing what percentage of the total number of schools in the State it represents. |

### Selective masking

A final customization that can add value to the Reports and Atlases is the use of selective masking. Given that both functionalities allow the program to parse through a field within a layer, it might be useful to have the program highlight the current area being zoomed in, or conversely that it fades out the ones around. To do this, the customization has to be done before the creation of the Atlas or the Report, using Rule-based formatting on the target layers. The desired effect can be done by assigning the desired style to the layer using the formula:

```attribute($currentfeature, 'admin2Name' = attribute(@atlas_feature, 'admin2Name')```

 and a more transparent version of it with the formula:

```attribute($currentfeature, 'admin2Name' != attribute(@atlas_feature, 'admin2Name')```

Note that on the formula the field used to parse is called 'admin2Name'.

**Figure 3. Example of selective masking with States in Myanmar**

![Figure 3](https://gitlab.iiep.unesco.org/g.vargas/preparing-reports-and-atlases-in-qgis/blob/main/assets/Figure_3.png?raw=true "Figure 3")

## Step 5. Review and publish

Once the final product is finished, it is up to the Ministry of Education team to go over it to make sure it contains all the desired information, and that everything, design- and content-wise, is up to a high standard. With the processing of the information being streamlined by the use of Atlases and Reports in QGIS, the production of more frequent and more tailored reports means that the Ministry of Education team can better respond to their internal and external needs with one fraction of the time and effort.

## Authors
Germán Vargas Mesa, Assistant Programme Specialist 
Amélie A. Gagnon, Senior Programme Specialist 

## License
See [LICENSE](https://gitlab.iiep.unesco.org/g.vargas/preparing-reports-and-atlases-in-qgis/-/blob/main/LICENSE).

## Disclaimer

The designations employed and the presentation of the material in this publication do not imply the expression of any opinion whatsoever on the part of UNESCO or IIEP concerning the legal status of any country, territory, city or area, or of its authorities, or concerning the delimitation of its frontiers or boundaries. International boundaries used in this paper come from the United Nations Geographic division, and subnational boundaries come from United Nations OCHA as available on the Humanitarian Data Exchange.

Was this technical guide useful? Let us know if you used it, and if we can make it better. 
Send us an email to development@iiep.unesco.org 

